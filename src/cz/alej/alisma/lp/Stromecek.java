package cz.alej.alisma.lp;

/*
 * Tento program vypisuje rekurzivne obsah aktualniho adresare.
 * Muzete jej vyzkouset timto zpusobem (pokud jej mate
 * naimportovany v Eclipse).
 *
 * java -cp out/ cz.alej.alisma.lp.Stromecek
 *
 * Prepiste metodu tiskniSoubory tak, aby byl stromecek adresaru
 * vypsan nasledujicim zpusobem:
 *
 *  |-- .classpath
 *  |-- src/
 *  |   `-- cz/
 *  |        `-- alej/
 *  |             `-- alisma/
 *  |                  `-- lp/
 *  |                       |-- Morseovka.java
 *  |                       `-- Stromecek.java
 *  |-- README.md
 *  |-- .project
 *  `-- out/
 *       `-- cz/
 *            `-- alej/
 *                 `-- alisma/
 *                      `-- lp/
 *                           |-- Stromecek.class
 *                           `-- Morseovka.class
 *
 */

import java.io.File;
import java.io.PrintStream;

public class Stromecek {
    public static void main(String[] args) {
        tiskniSoubory(new File("."), "", System.out);
    }

    private static void tiskniSoubory(File adresar, String odsazeni, PrintStream vystup) {
        File[] files = adresar.listFiles();
        for (int i = 0; i < files.length; i++) {
			File f = files[i];
			boolean posledni = (i == files.length - 1);
            vystup.printf("%s%s-- %s%s\n", odsazeni, posledni ? "`" : "|",f.getName(), f.isDirectory() ? "/" : "");
            if (f.isDirectory()) {
                tiskniSoubory(f, odsazeni + (posledni ? " " : "|") + "  ", vystup);
            }
        }
    }
}
