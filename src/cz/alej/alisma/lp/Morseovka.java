package cz.alej.alisma.lp;

/*
 * Tento program ma prepisovat text do Morseovy abecedy.
 *
 * Zatim ale umi jen nacitat jednotlive znaky na vstupu a
 * preskakovat tzv. bile znaky (mezery, znaky noveho radku
 * apod.).
 *
 * Doplnte implementaci metody prevedZnak, aby umela prevest
 * alespon vsechna pismena. Vyuzijte vhodnou tridu z Java
 * Collections pro ulozeni vlastniho mapovani mezi znakem
 * a jeho morseovou reprezentaci.
 *
 * Pri vstupu "Ahoj svete" ocekavame vystup
 *
 * .-/..../---/.---/.../...-/./-/./
 */

import java.util.Scanner;

public class Morseovka {
	private static final String ODDELOVAC = ".|\\n";
	private static final String NEZNAME = "??";
	private static Character[] pismena = new Character[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
			'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
	private static String[] morseovka = new String[] { ".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..",
			".---", "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-",
			"-.--", "--.." };

	private static String prevedZnak(Character znak) {
		if (Character.isWhitespace(znak)) {
			return null;
		}
		int pismeno = -1;
		for (int i = 0; i < pismena.length; i++) {
			if (pismena[i] == Character.toLowerCase(znak)) {
				pismeno = i;
				break;
			}

		}
		if (pismeno != -1) {
			return morseovka[pismeno];
		}
		return NEZNAME;
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		sc.useDelimiter("");
		while (sc.hasNext(ODDELOVAC)) {
			char znak = sc.next(ODDELOVAC).charAt(0);
			String morse = prevedZnak(znak);
			if (morse == null) {
				continue;
			}
			System.out.printf("%s/", morse);
		}
		sc.close();
		System.out.println();
	}

}
